# PKGBUILDs

This repository is a collection of some PKGBUILDs I maintain, that are not on the AUR for various reasons.

The PKGBUILDs are based on Arch repositories.

## Build instructions

Go to the package you want to build:

```
cd <packagename>
```

Then run this command to build and install the package:

```
makepkg -sci
```

This will pull in the misisng dependencies, build the package, clean up after it and lastly install the package prompting you for your password.

## Disclaimer

Using these packages are at your own risk. I do not take any responsibility for any issues that could arise from using these packages.

**You are on your own!**
